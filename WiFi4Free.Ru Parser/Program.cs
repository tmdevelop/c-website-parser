﻿namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class Program
    {
        public static void Main(string[] args)
        {
            Proxy proxies = null;
            if (args.Length > 0)
            {
                if (Proxy.ValidateFile(args[0]))
                {
                    proxies = new Proxy(args[0]);
                    try
                    {
                        if (args[1].Equals("--filter"))
                        {
                            proxies.FilterProxies(true);
                        }
                    }
                    catch
                    {
                    }
                }
                else
                {
                    Console.Write("Wrong arguments. Press any key to exit...");
                    Console.ReadKey();
                }
            }

            new Parser(proxies);
        }
    }
}
