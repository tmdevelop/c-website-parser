﻿namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using HtmlAgilityPack;
    using System.Globalization;
    using System.IO;

    internal class Parser
    {
        #region General parsing constants

        public const char NewLine = '\n';
        public const char Tab = '\t';
        public const char Carriage = '\r';
        private const string ElemLink = "a";

        #endregion

        #region Website Related Constants

        public const string Url = "http://wifi4free.ru";
        public const string SpotLinkAccessor = "hotspots";
        public const string PaginationAccessor = "page";

        #endregion

        #region XPath Selectors

        private const string CountrySelector = "//div[@id='towns-list-wrapper']/" +
                                            "ul/li[@class='country-toggler']";

        private const string CityLinkSelector = "following-sibling::li/ul/li";

        private const string SpotDetailsSelector =
            "//div[@class='content']/dl[@class='details-list']/dt";

        private const string SpotDetailsDDSiblingSelector = "following-sibling::dd";

        private const string SpotDetailsDescriptionSelector =
            "//div[@class='content']/div[@class='description']";

        private const string SpotLinkSelector =
                    "//table[@class='hotspots-table']//div[@class='address']/a";

        private const string PaginationSelector =
                                "//ul[@class='pagination']/li[position() > 1]";

        private const string ScriptTagSelector =
                                "//body/script";
        #endregion

        #region Text Label Selectors

        private const string LabelAdressText = "адрес";
        private const string LabelTypeText = "тип";
        private const string LabelFacilityText = "заведение";
        private const string LabelDateText = "дата";
        private const string LabelSocketText = "розетки";
        private const string LabelWorkingHoursText = "часы работы";

        #endregion

        #region Regex Selectors

        private const string FreeTypeStrRegex = @".*Бесплатн.*";
        private const string NoSocketStrRegex = @".*нет.*";
        private const string CityLatRegex = @"maplat.*=.*'(\d+\.*\d*)'";
        private const string CityLngRegex = @"maplng.*=.*'(\d+\.*\d*)'";
        private const string SpotLatRegex = @"spotlat.*=.*'(\d+\.*\d*)'";
        private const string SpotLngRegex = @"spotlng.*=.*'(\d+\.*\d*)'";

        #endregion

        #region Parsing constant vars

        private const int Timeout = 4000;
        private const string Backup = "backup.csv";

        #endregion

        #region Private Fields

        private ArrayList _countries;
        private List<City> _cities;
        private Dictionary<string, int> _spotsLinks;
        private List<WifiSpot> _spots;
        private HtmlWeb _webPage;
        private Database _db;

        #endregion

        public Parser(Proxy proxies = null)
        {
            _countries = new ArrayList();
            _cities = new List<City>();
            _spotsLinks = new Dictionary<string, int>();
            _spots = new List<WifiSpot>();
            _webPage = new HtmlWeb();
            _db = new Database();

            // Set the timeout and proxy randomization if proxies exist
            if (proxies != null && proxies.ProxyList.Count > 0)
            {
                _webPage.PreRequest = delegate(HttpWebRequest webRequest)
                {
                    webRequest.Timeout = Timeout;
                    try
                    {
                        webRequest.Proxy = proxies.PickRandom();
                    }
                    catch (OutOfProxiesException ex)
                    {
                        Debug.Log(ex.Message);
                        Console.WriteLine(
                            "Current parsed spots count {0}. Save current data to database? (Y/N)",
                            _spots.Count);
                        if (Console.ReadKey().Key == ConsoleKey.Y)
                        {
                            Console.WriteLine("Pressed Y");
                            Console.WriteLine("Saving...");
                            if (AddToDb())
                            {
                                Debug.Log("Saved.");
                            }
                            else
                            {
                                Debug.Log("Not Saved");
                            }

                            Console.WriteLine("Press any key to exit...");
                            Console.ReadKey();
                        }

                        Environment.Exit(0);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogToFile(ex.ToString());
                    }

                    return true;
                };
            }

            // Lets't parse the cities link
            Debug.LogLine("Parsing cities...");
            ParseCities(LoadPage(Parser.Url));

            // And the spot links for each city
            Debug.LogLine("Parsing spots links...");
            for (int i = 0; i < _cities.Count; i++)
            {
                ParseSpotsLinks(_cities.ElementAt(i), i);
            }

            // Finally let's parse all spots deatils for each 
            // spot link
            Console.Write("Parsing spots detail info...");
            for (int i = 0; i < _spotsLinks.Keys.Count; i++)
            {
                string val = _spotsLinks.Keys.ElementAt(i);
                Debug.LogLine(String.Format(
                                "Current Spot: {0} out of {1}",
                                i,
                                _spotsLinks.Count));

                ParseSpotInfo(val, _spotsLinks[val]);
            }

            if (!AddToDb())
            {
                Debug.Log("Error while adding to database.");
            }

            AddToFile();

            Debug.LogLine(String.Format(
                "Done! Total parsed spots: {0} out of {1} spots links",
                _spots.Count,
                _spotsLinks.Count
                ));
            Console.ReadKey();
        }

        /// <summary>
        /// Parses the spot link page for spot details
        /// </summary>
        /// <param name="spotLink"> </param>
        private void ParseSpotInfo(string spotLink, int cityID)
        {
            HtmlDocument spotPage = LoadPage(Url + spotLink);
            HtmlNodeCollection spotInfo = spotPage.DocumentNode.SelectNodes(
                                                  SpotDetailsSelector);
            HtmlNodeCollection spotDescriptionInfo = spotPage.DocumentNode.SelectNodes(
                                                        SpotDetailsDescriptionSelector);
            WifiSpot newSpot = new WifiSpot();
            newSpot.CityID = cityID;

            try
            {
                HtmlNodeCollection scripts = spotPage.DocumentNode.SelectNodes(ScriptTagSelector);
                MatchLatLng(scripts, newSpot);
                foreach (HtmlNode node in spotInfo)
                {
                    string nodeLabel = node.InnerText.ToLower().TrimEnd(Carriage, NewLine).Trim();
                    string nodeText = node.SelectNodes(SpotDetailsDDSiblingSelector)
                                                .First()
                                                .InnerText
                                                .TrimEnd(Carriage, NewLine)
                                                .Trim();
                    switch (nodeLabel)
                    {
                        case LabelAdressText:
                            newSpot.Address = nodeText;
                            break;
                        case LabelTypeText:
                            if (Regex.Match(nodeText, FreeTypeStrRegex).Success)
                            {
                                newSpot.SpotType = (int)WifiSpot.WifiType.FREE;
                            }
                            else
                            {
                                newSpot.SpotType = (int)WifiSpot.WifiType.PAID;
                            }

                            break;
                        case LabelFacilityText:
                            newSpot.Facility = nodeText;
                            break;
                        case LabelSocketText:
                            if (Regex.Match(nodeLabel, NoSocketStrRegex).Success)
                            {
                                newSpot.SocketAvailable = (int)WifiSpot.SocketType.UNAVAILABLE;
                            }
                            else
                            {
                                newSpot.SocketAvailable = (int)WifiSpot.SocketType.AVAILABLE;
                            }

                            break;
                        case LabelWorkingHoursText:
                            newSpot.FacilityWorkingHours = nodeText;
                            break;
                        case LabelDateText:
                            newSpot.DateAdded = nodeText;
                            break;
                    }
                }

                if (spotDescriptionInfo != null)
                {
                    newSpot.Discription = spotDescriptionInfo.First().InnerText;
                }

                _spots.Add(newSpot);
            }
            catch (Exception ex)
            {
                Debug.LogToFile(ex.ToString());
                Debug.LogToFile(Url + spotLink + "|" + cityID, "lost_spots.txt");
            }
        }

        /// <summary>
        /// Parses all cities links for all available countries
        /// </summary>
        private void ParseCities(HtmlDocument htmlDoc)
        {

            HtmlNodeCollection nodes = htmlDoc.DocumentNode.SelectNodes(CountrySelector);
            if (nodes != null)
            {
                int i = 0;
                foreach (HtmlNode country in nodes)
                {
                    _countries.Add(country.SelectNodes(ElemLink).First().InnerHtml);
                    HtmlNodeCollection cities = country.SelectNodes(CityLinkSelector);
                    foreach (HtmlNode linkNode in cities)
                    {
                        HtmlNode cityNode = linkNode.SelectNodes(ElemLink).First();
                        string properCityLink = cityNode.Attributes["href"].Value;
                        if (i > 0)
                        {
                            properCityLink = cityNode.Attributes["href"].Value;
                            int slashIndex = properCityLink.IndexOf('/', 1);
                            properCityLink = properCityLink.Substring(
                                                    slashIndex,
                                                    properCityLink.Length - slashIndex);
                        }

                        City city = new City()
                        {
                            CityName = cityNode.InnerHtml,
                            CountryID = i + 1,
                            CityLink = properCityLink
                        };

                        Debug.LogToFile(city.CityName);
                        _cities.Add(city);
                    }

                    i++;
                }

                Console.WriteLine("Done. Total cities parsed: {0}.", _cities.Count);
            }
            else
            {
                Console.Write("Couldn't find any cities. Press any key to exit...");
                Console.ReadKey();
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Recursively parses wifi-spot links for the specific city
        /// </summary>
        /// <param name="pageNum">number of a page to parse</param>
        /// <param name="cityLink">city link part (e.g. "/msk/")</param>
        /// <param name="pages">amount of pages of a specific city</param>
        private void ParseSpotsLinks(City city, int cityIndex, int pageNum = 1, int pages = -1, bool repeat = false)
        {
            HtmlDocument cityPage = LoadPage(Url + city.CityLink + SpotLinkAccessor +
                                                   "/" + PaginationAccessor + pageNum);
            HtmlNodeCollection spotLinks = cityPage.DocumentNode.SelectNodes(SpotLinkSelector);
            if (spotLinks != null)
            {
                foreach (HtmlNode link in spotLinks)
                {
                    HtmlAttribute spotLink = link.Attributes["href"];
                    _spotsLinks.Add(spotLink.Value, cityIndex + 1);
                }

                Console.WriteLine(
                            "Parsed {0} spot links. Current city {1}, page {2}.",
                            _spotsLinks.Count,
                            city.CityLink,
                            pageNum);
            }
            else
            {
                Console.WriteLine(
                            "Couldn't find spot links for city {0}, page number {1}",
                            city.CityLink,
                            pageNum);
                if (!repeat)
                {
                    ParseSpotsLinks(city, cityIndex, pageNum, pages, true);
                }
                else
                {
                    return;
                }
            }

            if (repeat)
            {
                return;
            }

            if (pageNum == 1)
            {
                HtmlNodeCollection scripts = cityPage.DocumentNode.SelectNodes(ScriptTagSelector);
                MatchLatLng(scripts, city);
            }

            if (pages == -1)
            {
                try
                {
                    pages = cityPage.DocumentNode.SelectNodes(PaginationSelector).Count;
                }
                catch (Exception)
                {
                    // Ignored 
                }
            }

            if (pages > 1 && pageNum != pages)
            {
                ParseSpotsLinks(city, cityIndex, ++pageNum, pages);
            }
        }

        private void MatchLatLng(HtmlNodeCollection scriptNodes, Object obj)
        {
            if (scriptNodes != null)
            {
                string latRegex = null;
                string lngRegex = null;

                if (typeof(WifiSpot) == obj.GetType())
                {
                    latRegex = SpotLatRegex;
                    lngRegex = SpotLngRegex;
                }
                else if (typeof(City) == obj.GetType())
                {
                    latRegex = CityLatRegex;
                    lngRegex = CityLngRegex;
                }
                else
                {
                    return;
                }

                foreach (HtmlNode script in scriptNodes)
                {
                    Match lat = Regex.Match(script.InnerHtml, latRegex, RegexOptions.IgnoreCase);
                    if (lat.Success)
                    {
                        double latitude = 0.0;
                        if (Double.TryParse(
                                        lat.Groups[1].Value,
                                        NumberStyles.Any,
                                        CultureInfo.InvariantCulture,
                                        out latitude))
                        {
                            if (obj.GetType() == typeof(WifiSpot))
                            {
                                ((WifiSpot)obj).SpotLat = latitude;
                            }
                            else
                            {
                                ((City)obj).CityLat = latitude;
                            }
                        }

                        Match Lng = Regex.Match(script.InnerHtml, lngRegex, RegexOptions.IgnoreCase);
                        if (Lng.Success)
                        {
                            double longitude = 0.0;
                            if (Double.TryParse(
                                    Lng.Groups[1].Value,
                                    NumberStyles.Any,
                                    CultureInfo.InvariantCulture,
                                    out longitude))
                            {
                                if (obj.GetType() == typeof(WifiSpot))
                                {
                                    ((WifiSpot)obj).SpotLng = longitude;
                                }
                                else
                                {
                                    ((City)obj).CityLng = longitude;
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }

        private bool AddToDb()
        {
            _db.CreateDatabase();

            bool success = true;
            if (!_db.InsertCountries(_countries))
            {
                success = false;
            }

            if (!_db.InsertCities(_cities))
            {
                success = false;
            }

            if (!_db.InsertSpots(_spots))
            {
                success = false;
            }

            return success;
        }

        private void AddToFile()
        {
            StringBuilder str = new StringBuilder();
            foreach (WifiSpot spot in _spots)
            {
                str.Append(spot);
            }

            File.WriteAllText(Backup, str.ToString() + Environment.NewLine);
        }

        private HtmlDocument LoadPage(string URL)
        {
            while (true)
            {
                try
                {
                    return _webPage.Load(URL);
                }
                catch
                {
                    Proxy.InvalidProxy = true;
                }
            }
        }
    }
}
