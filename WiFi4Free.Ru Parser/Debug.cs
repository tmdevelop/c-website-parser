﻿// -----------------------------------------------------------------------
// <copyright file="Debug.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    public static class Debug
    {
        private static string _debugFile = "debug.txt";

        public static string DebugFile { get; set; }

        public static void Log(string message)
        {
            Console.WriteLine(">>> {0}; {1}",
                        DateTime.Now.ToString("HH:mm:ss tt"),
                        message);
        }

        public static void LogLine(string message)
        {
            Console.WriteLine();
            Log(message);
        }

        public static void LogToFile(string message)
        {
            LogToFile(message, _debugFile);
        }

        public static void LogToFile(string message, string filePath)
        {
            int offset = 0;
            try
            {
                offset = Encoding.UTF8.GetBytes(File.ReadAllText(filePath, Encoding.UTF8)).Length;
            }
            catch
            {
            }

            try
            {
                using (FileStream fs = File.Open(
                                                filePath,
                                                FileMode.OpenOrCreate,
                                                FileAccess.ReadWrite))
                {
                    if (fs.CanWrite)
                    {
                        fs.Seek(offset, SeekOrigin.Begin);
                        byte[] array = Encoding.UTF8.GetBytes(
                                DateTime.Now.ToString(
                                "dd-MM-yyyy HH:mm:ss >>\t\t\t\t")
                                + message
                                + Environment.NewLine);
                        fs.Write(
                            array,
                            0,
                            array.Length);
                        fs.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
