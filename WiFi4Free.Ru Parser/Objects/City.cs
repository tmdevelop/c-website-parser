﻿namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class City
    {
        public int CountryID { get; set; }

        public string CityName { get; set; }

        public string CityLink { get; set; }

        public double CityLat { get; set; }

        public double CityLng { get; set; }
    }
}
