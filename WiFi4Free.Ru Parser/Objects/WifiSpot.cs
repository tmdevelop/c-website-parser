﻿namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class WifiSpot
    {
        public enum WifiType
        {
            FREE,
            PAID
        }

        public enum SocketType
        {
            AVAILABLE,
            UNAVAILABLE
        }

        public string Address { get; set; }

        public int SpotType { get; set; }

        public double SpotLat { get; set; }

        public double SpotLng { get; set; }

        public string Facility { get; set; }

        public string FacilityWorkingHours { get; set; }

        public int? SocketAvailable { get; set; }

        public string Discription { get; set; }

        public string DateAdded { get; set; }

        public int CityID { get; set; }

        public override string ToString()
        {
            return String.Format(
                "\"{0}\"|\"{1}\"|\"{2}\"|\"{3}\"|\"{4}\"" +
                "|\"{5}\"|\"{6}\"|\"{7}\"|\"{8}\"",
                Address,
                CityID,
                Facility,
                FacilityWorkingHours,
                SpotType,
                SocketAvailable,
                DateAdded,
                SpotLat,
                SpotLng);
        }
    }
}
