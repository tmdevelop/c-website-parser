﻿// -----------------------------------------------------------------------
// <copyright file="OutOfProxiesException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;


    public class OutOfProxiesException : Exception
    {
        public OutOfProxiesException(string exceptionMessage)
            : base(exceptionMessage)
        {

        }
    }
}
