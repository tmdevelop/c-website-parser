﻿namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Net.NetworkInformation;
    using System.Text.RegularExpressions;
    using HtmlAgilityPack;
    using System.Timers;

    internal class Proxy
    {
        private const int ProxyPingTimeOut = 2000;
        private const string UriToCheck = "http://93.158.134.3";

        private Timer _timer;
        private int _checkInterval = 1000 * 60 * 10;
        private bool _checkNeeded = false;
        private WebProxy _currentProxy;

        private List<WebProxy> _proxyList;
        private string _proxyPath;

        public static bool InvalidProxy { get; set; }

        public int ErrorCount { get; set; }

        public Proxy(string proxyFilePath)
        {
            _proxyList = new List<WebProxy>();
            LoadProxiesFromFile(proxyFilePath);
        }

        public static bool ValidateFile(string arg)
        {
            return Regex.Match(arg, @"\.txt", RegexOptions.IgnoreCase).Success;
        }

        private static bool Validate(WebProxy proxy)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UriToCheck);
            request.Proxy = proxy;
            request.Timeout = ProxyPingTimeOut;
            request.Method = "HEAD";

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response == null)
                    {
                        return false;
                    }

                    Debug.LogLine(String.Format(
                        "Status code: {0} ; Description: {1}.",
                        response.StatusCode,
                        response.StatusDescription));

                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch
            {
                return false;
            }
        }

        public void FilterProxies(bool rewriteOriginal = false)
        {
            Console.WriteLine("*** FILTERING PROXIES ***");
            if (_proxyList.Count > 0)
            {
                int initialProxyCount = _proxyList.Count;
                List<WebProxy> proxyClone =
                    new List<WebProxy>();

                List<string> fullProxyList = new List<string>();

                foreach (WebProxy proxyAddress in _proxyList)
                {
                    if (Proxy.Validate(proxyAddress))
                    {
                        proxyClone.Add(proxyAddress);
                        fullProxyList.Add(proxyAddress.Address.Authority);
                        Debug.LogToFile(proxyAddress.Address.Authority);
                    }
                }

                if (_proxyList.Count != proxyClone.Count)
                {
                    _proxyList = proxyClone;
                    if (rewriteOriginal)
                    {
                        File.WriteAllLines(_proxyPath, fullProxyList);
                    }
                }

                Debug.Log(String.Format(
                    "Filtered. New proxy list is {0} out of {1}",
                    _proxyList.Count,
                    initialProxyCount));
                return;
            }

            Debug.Log("No Proxies Found");
        }

        public List<WebProxy> ProxyList
        {
            get { return _proxyList; }
        }

        private bool LoadProxiesFromFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                _proxyPath = filePath;
                using (StreamReader proxies = File.OpenText(filePath))
                {
                    while (!proxies.EndOfStream)
                    {
                        try
                        {
                            string[] proxyHostPort = proxies.ReadLine().Split(':');
                            ProxyList.Add(
                                new WebProxy(
                                    proxyHostPort[0],
                                    Convert.ToInt32(proxyHostPort[1])));
                        }
                        catch
                        {
                            ErrorCount++;
                        }
                    }

                    if (_proxyList.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                Debug.Log("Proxy file doesn't exists");
                return false;
            }
        }

        public WebProxy PickRandom()
        {

            if (_proxyList.Count > 0)
            {
                if (Proxy.InvalidProxy)
                {
                    if (_currentProxy != null)
                    {
                        _proxyList.Remove(_currentProxy);
                    }

                    Proxy.InvalidProxy = false;
                }

                WebProxy proxy = _proxyList.ToArray()[new Random().Next(0, ProxyList.Count - 1)];
                _currentProxy = proxy;
                return proxy;
            }
            else
            {
                if (!LoadProxiesFromFile(_proxyPath))
                {
                    throw new OutOfProxiesException("No Proxies Working Proxies");
                }

                return PickRandom();
            }
        }



        public void SetTimer()
        {
            _timer = new Timer(_checkInterval);
            _timer.Elapsed += delegate(object src, ElapsedEventArgs args)
            {
                Debug.Log("Scheduled proxy checking.");
                _checkNeeded = true;
            };
            _timer.Start();
        }

        public static bool CanPing(string ip)
        {
            Ping ping = new Ping();

            try
            {
                PingReply reply = ping.Send(ip, ProxyPingTimeOut);
                if (reply == null)
                {
                    return false;
                }

                return reply.Status == IPStatus.Success;
            }
            catch
            {
                return false;
            }
        }

        //public bool GetProxies()
        //{
        //    HtmlWeb web = new HtmlWeb(); 
        //    web.PreRequest = delegate(HttpWebRequest webRequest) 
        //    {
        //        webRequest.Method = "GET";
        //        webRequest.CookieContainer = new CookieContainer();
        //        webRequest.CookieContainer.Add(new Cookie("from", "link","/", "www.freeproxylists.net"));
        //        webRequest.CookieContainer.Add(new Cookie("key", "key", "/", "www.freeproxylists.net"));
        //        webRequest.CookieContainer.Add(new Cookie("pv", "3", "/", "www.freeproxylists.net"));
        //        webRequest.CookieContainer.Add(new Cookie("userno", "20130126-007372", "/", "www.freeproxylists.net"));
        //        webRequest.CookieContainer.Add(new Cookie("__atuvc", "2%7C4", "/", "www.freeproxylists.net"));
        //        webRequest.CookieContainer.Add(new Cookie("refdomain", "www.freeproxylists.net", "/", "www.freeproxylists.net"));
        //        return true;
        //    };

        //    HtmlDocument htmlDoc = web.Load(ProxySourceUri);

        //    if (htmlDoc != null)
        //    {
        //        try
        //        {
        //            HtmlNodeCollection nodes = htmlDoc.DocumentNode.SelectNodes("//table[@class='DataGrid']/tr");
        //            foreach (HtmlNode htmlNode in nodes)
        //            {
        //                string address = htmlNode.FirstChild.LastChild.InnerHtml;
        //                string port = htmlNode.ChildNodes[1].InnerHtml;
        //                _proxyList.Add(address, int.Parse(port));
        //            }
        //        }
        //        catch
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}

        //public bool GetProxies()
        //{
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ProxySourceUri);
        //    request.Timeout = ProxyPingTimeOut;
        //    request.Method = "POST";
        //    request.Referer = ProxySourceUri;
        //    request.KeepAlive = true;
        //    request.UserAgent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.12";
        //    request.Host = "spys.ru";
        //    byte[] requestData = Encoding.UTF8.GetBytes(reqString);
        //    request.ContentLength = requestData.LongLength;

        //    try
        //    {
        //        using (Stream requestStream = request.GetRequestStream())
        //        {
        //            requestStream.Write(requestData, 0, requestData.Length);
        //            requestStream.Flush();
        //            requestStream.Close();

        //            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //            {
        //                if (response == null) return false;

        //                HtmlDocument htmlDoc = new HtmlDocument();
        //                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        //                {
        //                    htmlDoc.LoadHtml(reader.ReadToEnd());
        //                    return true;
        //                }

        //            }
        //        }

        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
    }

}
