﻿namespace Timz_Devz.Wifi4Free
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;

    public class HotKeyPressedListener
    {
        public delegate void OnSaveButtonPressed();

        public OnSaveButtonPressed SaveBtnPressedHandler { get; set; }

        public HotKeyPressedListener()
        {
            new Thread(new ThreadStart(ListerKeyBoardEvent));
        }

        private void ListerKeyBoardEvent()
        {
            do
            {   
                if (Console.ReadKey().Key == ConsoleKey.S)
                {
                    if (SaveBtnPressedHandler != null)
                    {
                        SaveBtnPressedHandler();
                    }
                }
            } while (true);
        }
    }
}
